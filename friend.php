<?php

session_start();
$id = $_GET['id'];
$idf = $_GET['idf'];
$idfmd5 = md5($idf);
$home = 'home.php?id='.$id;
$profil = 'profil.php?id='.$id;
$serch = 'serch.php?id='.$id."&serch=";
if ($_SESSION['usuario'] == "" || $id == "") {
	header('location: Error404.php');
}elseif ($idf == "") {
	header('location: '.$serch);
}
	
#----------------------------------------------------------------------------------------------------

$dir = "assets/img/";
$name = "assets/img/".$idfmd5.".png";
opendir($dir);
if (isset($_POST['send'])) {
	$destino = $name;
	copy($_FILES['foto']['tmp_name'], $destino);
	$name = "assets/img/".$id.".png";
}


#----------------------------------------------------------------------------------------------------

include 'conexion.php';

$sql = "SELECT * FROM usuarios WHERE NomUsu='$idf'";
$resultado = $conexion->query($sql) or die("fallo al obtener");
while ($row=$resultado->fetch_assoc()){
	$nom = $row['name']." ".$row['lastName'];
	$gender = $row['gender'];
	$cumple = $row['Date'];
	$email = $row['email'];
}
if ($gender == 1) {
	$gender = "Male";
}elseif ($gender == 2) {
	$gender = "Female";
}elseif ($gender == 3) {
	$gender = "I wouldn't want to say it";
}

#----------------------------------------------------------------------------------------------------

if ($_GET['am'] == "no") {
	$query2="SELECT * FROM `amigosde$id`";
	$rest=$conexion->query($query2);
	while ($row=$rest->fetch_assoc()){
		if ($row['Amigo']==$idf) {
			header("location: friend.php?id=".$id."&idf=".$idf."&am=si");
		}
	}
}

#----------------------------------------------------------------------------------------------------
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <script type="text/javascript">
      document.addEventListener('DOMContentLoaded', function() {
	    var elems = document.querySelectorAll('.parallax');
	    var instances = M.Parallax.init(elems);
	    var elems = document.querySelectorAll('.dropdown-trigger');
    	var instances = M.Dropdown.init(elems, {
    		coverTrigger: false,
    		constrainWidth: false
    	});
    	var elems = document.querySelectorAll('.modal');
    	var instances = M.Modal.init(elems);
	  });
    </script>

	<meta charset="utf-8">
	<title><?php echo $_SESSION['usuario']?></title>
	<link href="https://fonts.googleapis.com/css?family=Special+Elite&display=swap" rel="stylesheet">
	<style type="text/css">

	</style>
</head>
<body>
	<nav class="teal">

        <div class="nav-wrapper container">

	        <a href="<?=$home?>" class="brand-logo"><?php echo $_SESSION['usuario']?></a>

	        <ul id="nav-mobile" class="right">
	            <li><a class='dropdown-trigger' href='#' data-target='dropdown1'><i class="material-icons">more_vert</i></a></li>
	        </ul>

	        <ul id='dropdown1' class='dropdown-content'>
	        	<li><a href="<?=$profil?>"><i class="material-icons">account_circle</i><?php echo $_SESSION['usuario']?></a></li>
	        	<li><a href="<?=$serch?>"><i class="material-icons">person_add</i> Buscar</a></li>
	        	<li><a href="cerrar.php"><i class="material-icons">exit_to_app</i> Cerrar</a></li>
			</ul>

	    </div>
	</nav>

	<div class="parallax-container">
      <div class="parallax">
      	<img src="assets/img/tapiz.jpg">
      </div>
      <br>
      <br>
      <center>
      	<img style="width: 200px; height: 200px; opacity: .8; border-radius: 50%; border: 1px solid;" src="<?php echo $name; ?>">
      </center>
      <br>
      <h1 align="center" style="font-size: 70px; font-family: 'Special Elite', cursive; opacity: .9; color: #47ABCA;" class=""><?php echo $idf?></h1>
    </div>
    <br>
    <br>
    <div class="row">
    <div class="col s12 m12 l1 ">
    	
    </div>
    	<div class="col s12 m12 l4 ">
      <div class="card-panel teal">
        <span class="white-text ">
			<h5 style="font-family: 'Special Elite', cursive;">Info: </h5>
			<br>
        	<h6 style="font-family: 'Special Elite', cursive;"><?php echo $nom;?></h6>
        	<br>
        	<h6 style="font-family: 'Special Elite', cursive;"><?php echo $gender;?></h6>
        	<br>
        	<h6 style="font-family: 'Special Elite', cursive;"><?php echo $cumple;?></h6>
        	<br>
        	<h6 style="font-family: 'Special Elite', cursive;"><?php echo $email;?></h6>
        	<br>
        	<?php
        	if ($_GET['am']=="si") {
        		
        	?>
        	<a href="<?php echo'friend.php?id='.$id.'&idf='.$row['NomUsu'].'&am=si';?>" disabled class="btn teal-dark">Agreagar</a>
        	<?php
			}else{
			?>
        	<a href="<?php echo'add.php?id='.$id.'&addto='.$idf;?>" class="btn teal-dark">Agreagar</a>
			<?php
			}
        	?>
        </span>
      </div>
      <div class="card-panel teal">
        <span class="white-text ">

        </span>
      </div>
    </div>
    <center>
    <div class="col s0 m2 l0 ">
    	
    </div>
    <div class="col s12 m8 l6">
      <div class="card-panel teal">
        <span class="">
        	<div class="card">
		        <div class="card-image">
		        	<br>
		          <center><img style="width: 80%; height: 430px;" src="<?php echo $name; ?>"></center>
		        </div>
		        <div class="card-content">
		          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		        </div>
		        <div class="card-action">
		          <a id="publication" onclick="favorite()" class="grey-text"><i class="material-icons">favorite</i></a>
		        </div>
		        <script type="text/javascript">
		        	function favorite() {
		        		var element = document.getElementById("publication");
  							element.classList.toggle("teal-text");
  							element.classList.toggle("grey-text");
		        	}
		        </script>
		      </div>
        </span>
      </div>
    </div>	
    </center>
    <div class="col s12 m12 l1 ">
    	
    </div>
  </div>

	<div id="modal" class="modal">
		<form method="post" action="#" enctype="multipart/form-data">
			<div class="modal-content">
		    	<center>
			    	<h4>Cambia tu foto de perfil</h4>
			    	<br>
			    	<br>
			    	<img style="width: 400px; height: 400px; opacity: 1; border: 1px solid;" src="<?php echo $name; ?>">
			    	<br>
			    	<br>
			    	<br>
			    	<input type="file" name="foto">
		    	</center>
			</div>
			<div class="modal-footer">
	    		<input type="submit" class="modal-close waves-effect waves-green btn-flat" name="send">
			</div>
		</form>
	</div>
</body>
</html>