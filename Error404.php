<?php

	session_start();

?>

<!DOCTYPE html>
<html>
<head>
	<!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <script type="text/javascript">
      document.addEventListener('DOMContentLoaded', function() {
	    var elems = document.querySelectorAll('.parallax');
	    var instances = M.Parallax.init(elems);
	    var elems = document.querySelectorAll('.dropdown-trigger');
    	var instances = M.Dropdown.init(elems, {
    		coverTrigger: false
    	});
	  });
    </script>

	<meta charset="utf-8">
	<title>Error 404</title>
	<link href="https://fonts.googleapis.com/css?family=Special+Elite&display=swap" rel="stylesheet">
</head>
<body>
	<nav class="teal">

	    <div class="nav-wrapper container">

	        <ul id="nav-mobile" class="right hide-on-med-and-down">
	            <li><a class='dropdown-trigger' href='#' data-target='dropdown1'><i class="material-icons">more_vert</i></a></li>
	        </ul>

	        <ul id='dropdown1' class='dropdown-content'>
	        	<li><a href="cerrar.php"><i class="material-icons">exit_to_app</i> Cerrar</a></li>
			</ul>

	    </div>

	</nav>
	<center>
		<br>
		<br>
		<h1 style="font-family: 'Special Elite', cursive;">ERROR 404</h1>
		<br>
		<h2 style="font-family: 'Special Elite', cursive;">Pagina no encontrada</h2>
		<br>
		<div>
			<img src="https://image.flaticon.com/icons/png/512/1060/1060941.png" style="width: 200px; height: 200px;">
		</div>
	</center>
</body>
</html>