<?php
session_start();
$id = $_GET['id'];
$home = 'home.php?id='.$id;
$profil = 'profil.php?id='.$id;
$serch = 'serch.php?id='.$id."&serch=";
if ($_SESSION['usuario'] == "" || $id == "") {
	header('location: Error404.php');
}
	
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <script type="text/javascript">
      document.addEventListener('DOMContentLoaded', function() {
	    var elems = document.querySelectorAll('.parallax');
	    var instances = M.Parallax.init(elems);
	    var elems = document.querySelectorAll('.dropdown-trigger');
    	var instances = M.Dropdown.init(elems, {
    		coverTrigger: false,
    		constrainWidth: false
    	});
	  });
    </script>

	<meta charset="utf-8">
	<title><?php echo $_SESSION['usuario']?></title>
	<link href="https://fonts.googleapis.com/css?family=Special+Elite&display=swap" rel="stylesheet">
	<style type="text/css">

	</style>
</head>
<body>
		<nav class="teal">

        <div class="nav-wrapper container">

	        <a href="<?=$home?>" class="brand-logo"><?php echo $_SESSION['usuario']?></a>

	        <ul id="nav-mobile" class="right">
	            <li><a class='dropdown-trigger' href='#' data-target='dropdown1'><i class="material-icons">more_vert</i></a></li>
	        </ul>

	        <ul id='dropdown1' class='dropdown-content'>
	        	<li><a href="<?=$profil?>"><i class="material-icons">account_circle</i><?php echo $_SESSION['usuario']?></a></li>
	        	<li><a href="<?=$serch?>"><i class="material-icons">person_add</i> Buscar</a></li>
	        	<li><a href="cerrar.php"><i class="material-icons">exit_to_app</i> Cerrar</a></li>
			</ul>

	    </div>
	</nav>
	<br>
	<br>
	<br>
	<form method="post" action="#" class="container">
		<input type="text" name="serch">
		<input type="submit" class="btn teal" name="">
	</form>
	<br>
	<br>
  <table class="highlight container">
    <thead>
      <tr>
          <th>#</th>
          <th></th>
          <th>Name</th>
          <th>User</th>
          <th>E-mail</th>
          <th>Perfil</th>
          <th>Agregar</th>
      </tr>
    </thead>

  <?php
  if (isset($_POST['serch']) && $_POST['serch'] != "") {
  	
  include 'conexion.php';
  $busqueda = $_POST['serch'];
	$sql = "SELECT * FROM usuarios WHERE name LIKE '$busqueda%'";
	$resultado = $conexion->query($sql) or die("fallo al obtener");
	$i = 0;
	while ($row=$resultado->fetch_assoc()){
		$usumd5 = md5($row['NomUsu']);
		$img = "assets/img/".$usumd5.".png";
		$i = $i+1;
		?>
    <tbody>
      <tr>
        <td><?php echo $i ;?></td>
        <td><img style="width: 45px; height: 50px; opacity: .8;" src="<?php echo $img; ?>"></td>
        <td><?php echo $row['name']." ".$row['lastName']; ?></td>
        <td><?php echo $row['NomUsu']; ?></td>
        <td><?php echo $row['email']; ?></td>
        <td><a class="text-teal" href="<?php echo'friend.php?id='.$id.'&am=no'.'&idf='.$row['NomUsu'];?>">ver</a></td>
      </tr>
    </tbody>
    <?php }} ?>
  </table>

</body>
