<?php

session_start();
$id = $_GET['id'];
$home = 'home.php?id='.$id;
$profil = 'profil.php?id='.$id;
$serch = 'serch.php?id='.$id."&serch=";
if ($_SESSION['usuario'] == "" || $id == "") {
	header('location: Error404.php');
}
	
#----------------------------------------------------------------------------------------------------

$dir = "assets/img/";
$name = "assets/img/".$id.".png";
opendir($dir);
if (isset($_POST['send'])) {
	$destino = $name;
	copy($_FILES['foto']['tmp_name'], $destino);
	$name = "assets/img/".$id.".png";
}


#----------------------------------------------------------------------------------------------------


?>
<!DOCTYPE html>
<html>
<head>
	<!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <script type="text/javascript">
      document.addEventListener('DOMContentLoaded', function() {
	    var elems = document.querySelectorAll('.parallax');
	    var instances = M.Parallax.init(elems);
	    var elems = document.querySelectorAll('.dropdown-trigger');
    	var instances = M.Dropdown.init(elems, {
    		coverTrigger: false,
    		constrainWidth: false
    	});
    	var elems = document.querySelectorAll('.modal');
    	var instances = M.Modal.init(elems);
	    });
    </script>

	<meta charset="utf-8">
	<title><?php echo $_SESSION['usuario']?></title>
	<link href="https://fonts.googleapis.com/css?family=Special+Elite&display=swap" rel="stylesheet">
	<style type="text/css">

	</style>
</head>
<body>
	<nav class="teal">
    <div class="nav-wrapper container">

      <a href="<?=$home?>" class="brand-logo"><?php echo $_SESSION['usuario']?></a>

      <ul id="nav-mobile" class="right">
          <li><a class='dropdown-trigger' href='#' data-target='dropdown1'><i class="material-icons">more_vert</i></a></li>
      </ul>

      <ul id='dropdown1' class='dropdown-content'>
      	<li><a href="<?=$profil?>"><i class="material-icons">account_circle</i><?php echo $_SESSION['usuario']?></a></li>
      	<li><a href="<?=$serch?>"><i class="material-icons">person_add</i> Buscar</a></li>
      	<li><a href="cerrar.php"><i class="material-icons">exit_to_app</i> Cerrar</a></li>
			</ul>
    </div>
	</nav>

	<div class="parallax-container">
      <div class="parallax">
      	<img src="assets/img/tapiz.jpg">
      </div>
      <br>
      <br>
      <center>
      	<a class="modal-trigger" href="#modal"><img style="width: 200px; height: 200px; opacity: .8; border-radius: 50%; border: 1px solid;" src="<?php echo $name; ?>"></a>
      </center>
      <br>
      <h1 align="center" style="font-size: 70px; font-family: 'Special Elite', cursive; opacity: .7; color: #47ABCA;" class=""><a href="<?=$home?>" class="brand-logo"><?php echo $_SESSION['usuario']?></a></h1>
    </div>
    <br>
    <br>
    <div class="row">
    <div class="col s12 m12 l4 ">
      <div class="card-panel teal">
        <span class="white-text ">
					<h5 style="font-family: 'Special Elite', cursive;">Info: </h5>
					<br>
        	<h6 style="font-family: 'Special Elite', cursive;"><?php echo $_SESSION['nombre']." ".$_SESSION['last'];?></h6>
        	<br>
        	<h6 style="font-family: 'Special Elite', cursive;"><?php echo $_SESSION['gender'];?></h6>
        	<br>
        	<h6 style="font-family: 'Special Elite', cursive;"><?php echo $_SESSION['cumple'];?></h6>
        	<br>
        	<h6 style="font-family: 'Special Elite', cursive;"><?php echo $_SESSION['email'];?></h6>
        </span>
      </div>
    </div>
    <div class="col s12 m6 l6 ">
      <div class="card-panel teal">
        <span class="white-text">
          <div class="card">
            <div class="card-image">
              <br>
              <div class="card-content grey-text">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
              <br>
              <center><img style="width: 80%; height: 430px;" src="<?php echo $name; ?>"></center>
              <br>
            </div>
            <div class="card-action">
              <!-- <a id="publication" onclick="favorite()" class="grey-text"><i class="material-icons">favorite</i></a> -->
              <button id="publication" onclick="favorite()" class="btn-flat grey" style="border-radius: 50%; width: 49px; height: 50px;">+1</button>
            </div>
            <script type="text/javascript">
              function favorite() {
                var element = document.getElementById("publication");
                element.classList.toggle("teal");
                element.classList.toggle("grey");
              }
            </script>
          </div>
        </span>
      </div>
    </div>
    <div class="col s12 m6 l2 ">
      <div class="card-panel teal">
        <span class="white-text ">
    	<?php
		include('conexion.php');
		
		$query1="SELECT * FROM `amigosde$id`";
		$rest=$conexion->query($query1);
			while($row=$rest->fetch_assoc()){
				?>
				<a href="<?php echo'friend.php?id='.$id.'&am=si&idf='.$row['Amigo'];?>" style="color: aliceblue; text-decoration: none;"><h6 style="font-family: 'Special Elite', cursive;"><?php echo $row['Amigo'];?></h6></a>
				<br>
				<?php
			}
		?>
        </span>
      </div>
    </div>
    <center>
<!-- inicio del post
    <div class="col s12 m8 l6">
      <div class="card-panel teal">
        <span class="">
        	<div class="card">
		        <div class="card-image">
		        	<br>
		          <center><img style="width: 80%; height: 430px;" src="<?php echo $name; ?>"></center>
		        </div>
		        <div class="card-content">
		          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		        </div>
		        <div class="card-action">
		          <a id="publication" onclick="favorite()" class="grey-text"><i class="material-icons">favorite</i></a>
		        </div>
		        <script type="text/javascript">
		        	function favorite() {
		        		var element = document.getElementById("publication");
  							element.classList.toggle("teal-text");
  							element.classList.toggle("grey-text");
		        	}
		        </script>
		      </div>
        </span>
      </div>
    </div> 
    fin del post -->	
    </center>
    <div class="col s12 m12 l1 ">

    </div>
  </div>

	<div id="modal" class="modal">
		<form method="post" action="#" enctype="multipart/form-data">
			<div class="modal-content">
		    	<center>
			    	<h4>Cambia tu foto de perfil</h4>
			    	<br>
			    	<br>
			    	<img style="width: 400px; height: 400px; opacity: 1; border: 1px solid;" src="<?php echo $name; ?>">
			    	<br>
			    	<br>
			    	<br>
			    	<input type="file" name="foto">
		    	</center>
			</div>
			<div class="modal-footer">
	    		<input type="submit" class="modal-close waves-effect waves-green btn-flat" name="send">
			</div>
		</form>
	</div>
</body>
</html>